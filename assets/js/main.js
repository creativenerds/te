$(document).ready(function(){

    // Add animation to product page navigation
    $("#navbar li a").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 2000);
    });

    $('.top').click(function(e){
    	e.preventDefault();
    	$('html, body').animate({
    		scrollTop: 0
    	}, 2000);
    });
    /// END Add animation to product page navigation

    $('body').scrollspy();

});
